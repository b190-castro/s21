console.log("Hello World");

/*
	try to store the following inside a variable and log the variable in the console
		98.5
		94.3
		89.2
		90.1

	send an output in the google chat
*/

/*
	Array
		Arrays are used to store multiple related values in a single variable
		- They are declared using declared brackets ([]) also know as "Array Literals"
		- Arrays also provide access to a number of functions/methods that help in achieveing tasks that we can perform on the elements inside the arrays

		- Arrays are used to store numerous amounts of data t0 manipulate in order to perform methods

		- Methods are used similar to functions associated with objects

		- Arrays are also referred to as objects which is another data type.
		The only difference between the two is that arrays contain information of list, but an object uses "properties" and "values" in its elements.

		SYNTAX:

		let/const arrayName = [elementA,elementB,elementC,...elementN]
*/
let sampleScore = [98.5,94.3,89.2,90.1];
console.log(sampleScore);

// It is important that we atore related values inside an array so that its variable can live up to its description of its value
let computerBrands = ["Lenovo","Dell","Asus","HP","MSI","Acer","CDR-King"];
console.log(computerBrands);

// This is possible but, since we only provide list i arrays, this method of writing is not recommended
let mixedArr = [12, "Asus", null, undefined];
console.log(mixedArr);

// Reassign of array values
console.log("array before Reassigining: ");
console.log(mixedArr);
// acccessing the elements requires arrayName + index enclosed in square brackets
mixedArr[0] = "Hello World";
console.log("Array after reassigining: ");
console.log(mixedArr);

// SECTION - Reading from Arrays
/*
	accessing arrays elements is one of the more common tasks that we do with an array this can be done through the use of array indeces
		index - term used to declare the position of an element in an array.
		in JS, the first element is associated with number 0, incrementing as the number of the elements increase.

	SYNTAX:
		ArrayName[Index];
*/
console.log(sampleScore[0]);
console.log(computerBrands[3]);
console.log(computerBrands[6]);
// how we can get the number of elements in an array
console.log(computerBrands.length);
// accessing an index that is not existing would return undefinedd because this means that the element is not there
console.log(computerBrands[7]);

// One of the common use for the .length property
if (computerBrands.length>5){
	console.log("We have too many suppliers. Please coordinate with the operations manager");
}

// Since the first element of an array starts at 0, subtracting 1 from the length will offset the value by one allowing us to get the last index of the array, in case we forgot the number of its element
let lastElementIndex = computerBrands.length -1;
console.log(computerBrands[lastElementIndex]);

//SECTION - Array Methods
/*
	methods are similar to function, these array methods can be done in array and objects alone
*/
// Mutator Methods
/*
	- Mutator Methods - are function that "mutated" or change/alter an array after they have been created
	- These methods manipulate the original array perforing various tasks such as adding and removing elements
*/
let fruits = ["Apple","Mango","Rambutan","Lanzones","Durian"];
console.log(fruits);

// Push() - adds an element at the end of the array
/*
	SYNTAX:
		arrayName.push()
*/
let fruitsLength = fruits.push ("Mango");
console.log(fruitsLength);
console.log("Mutated Array from Push Method");
console.log(fruits);

/*fruits[6] = "Orange"
console.log(fruits);

the code above can also be written like this*/

fruits.push("Orange");
fruits.push("Guava","Avocado");
console.log("Mutated Array from Push Method.");
console.log(fruits);

// pop() - removes an element at the end of the array
/*
	SYNTAX:
		arrayName.pop()
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from Pop Method");
console.log(fruits);

fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from Pop Method");
console.log(fruits);

// unshift - adds an element at the start of an array
/*
	SYYNTAX:
		arrayName.unshift(element);
*/

fruits.unshift("Lime","Banana");
/*
	separating two unshifts will result into the second element being at the index 0 of the array fruits.unshift("Banana")
*/
console.log("Mutated Array from Push Method");
console.log(fruits);

// shift - removes an element from the start of the array
/*
	arrayName.shift();
*/
let fruitRemoved = fruits.shift();
console.log("Mutated Array from Shift Method");
console.log(fruits);

fruits.shift();
console.log("Mutated Array from Shift Method");
console.log(fruits);

// splice - simultaneously removes and adds from a specified index number.
/*
	SYNTAX:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(2,2,"Lime","Cherry","Strawberry");
console.log("Mutated Array from Splice Method");
console.log(fruits);

// sort - rearranges the elements in an array in alphanumeric order
/*
	SYNTAX:
		arrayName.sort()
*/
fruits.sort();
console.log("Mutated Array from Sort Method");
console.log(fruits);

// reverse - reverses the order of array
/*
	SYNTAX:
		arrayName.reverse()
*/
fruits.reverse();
console.log("Mutated Array from Reverse Method");
console.log(fruits);

// SECTION - Non-Mutator Methods
/*
	- are functions that do not modify the original array
	- these methods do not manipulate the elements inside the array even they are performing tasks such as returning elements from an aray and combining them with other arrays and printing the output
*/


let countries = ["RUS","CH","JPN","PH","USA","KOR","AUS","CAN","PH"]
// indexOf()

/*
	- return the index of the first matching element found in an array
	- the search process will start from the first element down to the last

	SYNTAX:
		arrayName.indexOf(searchValue);
*/
let firstIndex = countries.indexOf("PH")
console.log("Result of IndexOf: " + firstIndex);
firstIndex = countries.indexOf("KAZ");
//returns -1 for non-existing elements
console.log("Result of IndexOf: " + firstIndex);

// lastIndexOf()
/*
	starts the search process from the last element down to the first

	SYNTAX:
		arrayName.lastIndexOf(searchValue);
*/
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf: " + lastIndex);
lastIndex = countries.lastIndexOf("KAZ");
console.log("Result of lastIndexOf: " + lastIndex);

// Slice
/*
	copies a part of the array and returns a new array:

	SYNTAX:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/
let slicedArrayA = countries.slice(2);
console.log("Result of Slice: " + slicedArrayA);
console.log(countries);
// slicing the index 4 up until the 7th element
let slicedArrayB = countries.slice(4, 7);
console.log("Result of Slice: " + slicedArrayB);
console.log(countries);
// slicing off the elements from the end of the array
let slicedArrayC = countries.slice(-3);
console.log("Result of Slice: " + slicedArrayC);
console.log(countries);

// toString()
/*
	- returns a new array as a string separated by commas
	SYNTAX:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log("Result of toString: "/* + stringArray*/);
console.log(stringArray);
// console.log(countries);

// concat()
/*
	- used to combine two arrays and returns the combined result
	SYNTAX:
		arrayA.concat(arrayB);
*/
let tasksA = ["drink HTML","eat Javascript"];
let tasksB = ["inhale CSS","breathe SASS"];
let tasksC = ["get GIT","be node"];

let tasks = tasksA.concat(tasksB);
console.log("Result of concat: ");
console.log(tasks);

// combining multiple arrays
let allTasks = tasksA.concat(tasksB, tasksC);
console.log("Result of concat: ");
console.log(allTasks);

// join()
/*
	-
*/
let users = ["John","Jane","Joe","Jobert","Julius"];
console.log(users.join());
console.log(users.join(' '));
console.log(users.join('-'));

// Iterator methods
/*
	- Iteration methods are loops designed to perform repetitive tasks on arrays
	- Useful for manipulating array data resulting in complex tasks
*/

// forEach
/*
	- similar to a for-loop that loops through all elements
	- variable names for arrays are usually written in plural form of the data stored in an array

	it's common practice to use the singular form of the array content for parameter names used in array loops

	- array iteration normally work with a function supplied as an argument

	- how these function works is by performing tasks that are predefined within the array's method

	SYNTAX:
		arrayName.forEach(function(individualElement){
			statement/s
		})
*/
allTasks.forEach(function(task){
	console.log(task);
})

// forEach with conditional statements
let filteredTasks = []

allTasks.forEach(function(task){
	if (task.length > 10){
		// we stored the filtered elements inside another variable to avoid confusion should we need the original array in tact
		filteredTasks.push(task);
	}
})

console.log("Result of forEach: ");
console.log(filteredTasks);

// map
/*
	- iterates on each element AND returns a new array with different values depending on the result of the function's operation.
	- this is useful for performing tasks where mutating / changing elements are required
	SYNTAX:
		let/const resultArray = arrayName(function(individualElement){
			return statement
		})
*/
let numbers = [1,2,3,4,5];

let numbersMap = numbers.map(function(number){
	// unlike forEach, return statement is needed in map method to create a value that is stored in another array
	return number*number;
})
console.log("Result of map: ");
console.log(numbersMap);

//every()
/*
	- checks all elements pass the given condition
	- returns a boolean data taype depending if all elements meet the condition (true) or not (false)
	SYNTAX:
		let/const result = arrayName.every(function(individualElement){
			return expression/condition
		})
*/
let allValid = numbers.every(function(number){
	return (number <3);
})
console.log("Result of every: ");
console.log(allValid);
console.log(numbers);

//some()
/*
	- checks if at least one element in the array passes the given condition
	- returns boolean depending on the result of the function
	SYNTAX:
		let/const resultName = arrayName.some(function(individualElement){
			return expression/condition
		})
*/
let someValid = numbers.some(function(number){
	return (number<2);
})
console.log("Result of some: ");
console.log(someValid);
console.log(numbers);

// filter
/*
	- returns a new array that contains copies of the elements which meet the given condition
	- returns empty array if there are no eleemnts that meet the condition
	- usueful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration method such as forEach + if statement + push in an earlier example

	SYNTAX:
		let/const resultName = arrayName.fulter(function(individualElement){
			return expression/condition
		})
*/

let filterValid = numbers.filter(function(number){
	return (number<3);
})
console.log("Result of filter: ");
console.log(filterValid);
console.log(numbers);

let parts = ["Mouse","Keyboard","Unit","Monitor"];
console.log(parts);

//reduce()
/*
	evaluates elements from left to right and returns/reduces the array into a single value

	SYNTAX:
	let/const resultName = arrayName.reduce(functional(accumululator,currentValue){
		return expression/operation;
	})

	- accumulator - stores the result for every iteration
	- currentValue - is the next/current element in the Array that is evaluated in each iteration
*/

let iteration = 0;
let reducedArray = numbers.reduce(function(x,y){
	console.warn(iteration);
	console.log(x);
	console.log(y);
	return x+y;
})
console.log(reducedArray);

// using array of strings
// when used in strings, it returns the conactinated strings of the array
let reducedStringArray = parts.reduce(function(x,y){
	console.warn(iteration);
	console.log(x);
	console.log(y);
	return x+y;
})
console.log(reducedStringArray);

// includes method
/*
	- searches each of the elements if they have the specific character that is inside the element
*/
let filteredParts = parts.filter(function(part){
	return part.toLowerCase().includes("n");
});
console.log(filteredParts);